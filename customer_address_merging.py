from sqlalchemy import create_engine,func
from sqlalchemy.orm import sessionmaker, scoped_session
from model.CustomerAddress import CustomerAddress
from model.Address import Address
from model.CustomerScheduleAddress import CustomerScheduleAddress
from utils.AddressUtils import calculate_addresses_similarity, calculate_addresses_distance
import logging
from concurrent.futures import ThreadPoolExecutor

logging.basicConfig(filename='merging.log', level=logging.INFO, filemode='a')
MAX_DISTANCE = 1000
MIN_SIMILARITY = 0.5
engine = create_engine("postgresql://root:address_amaken-locations@address-analysis.caoclfy0asde.eu-west-1.rds"
                       ".amazonaws.com/address_amaken", encoding='utf8')

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)


def compare_address(addr1, addr2):
    """
    :param addr1:
    :param addr2:
    :return: True if both addresses are identical
    """
    loc_dist = calculate_addresses_distance(addr1.latitude, addr1.longitude, addr2.latitude, addr2.longitude)
    if loc_dist <= MAX_DISTANCE:
        address_line_sim = calculate_addresses_similarity(addr1.address_line, addr2.address_line)
        if address_line_sim >= MIN_SIMILARITY:
            log(addr1, addr2, loc_dist, address_line_sim)
            return True
    return False


def log(addr1, addr2, dist, sim):
    logging.info("Merging with Sim :" + str(sim) + ", Dist : " + str(dist))
    logging.info(addr1.address_line)
    logging.info(addr2.address_line)
    logging.info("--------------------------------------------")


def fetch_customer_address_records(session, customer_ids):
    """ Fetch records for list of  customer ids
     return dict of (cust_id and addr_id => customer_address record) and dict of customer => address ids"""
    customer_addresses_relation_tuple_dict = dict()
    customer_address_ids_dict = dict()
    cust_addr_list = session.query(CustomerAddress).filter(CustomerAddress.customer_id.in_(customer_ids)).all()
    for customer_address in cust_addr_list:
        customer_address_tuple = (customer_address.customer_id, customer_address.address_id)
        customer_addresses_relation_tuple_dict[customer_address_tuple] = customer_address
        if customer_address.customer_id in customer_address_ids_dict:
            customer_address_ids_dict[customer_address.customer_id].append(customer_address.address_id)
        else:
            customer_address_ids_dict[customer_address.customer_id] = [customer_address.address_id]

    return customer_addresses_relation_tuple_dict, customer_address_ids_dict


def merge_customer_addresses(customer_id, customer_address_ids_dict, customer_addresses_relation_tuple_dict):
    customer_address_ids = customer_address_ids_dict[customer_id]
    session = Session()
    addresses = session.query(Address).filter(Address.id.in_(customer_address_ids)).all()

    address_count = len(addresses)
    for i in range(0, address_count - 1):
        removed_indices = []
        for j in range(i + 1, address_count):
            try:
                addr1 = addresses[i]
                addr2 = addresses[j]
                is_identical = compare_address(addr1, addr2)
                if is_identical:
                    if len(addr1.address_line) < len(addr2.address_line):
                        # swap
                        addresses[i] = addr2
                        addresses[j] = addr1

                    # remove address in index j
                    removed_indices.append(j)
                    original_addr = addresses[i]
                    removed_addr = addresses[j]
                    original_customer_address = customer_addresses_relation_tuple_dict[(customer_id, original_addr.id)]
                    removed_customer_address = customer_addresses_relation_tuple_dict[(customer_id, removed_addr.id)]

                    # Update Customer Schedule record for removed address to original
                    try:
                        session.query(CustomerScheduleAddress). \
                            filter(CustomerScheduleAddress.customer_address_id == removed_customer_address.id). \
                            update({"customer_address_id": (original_customer_address.id)})
                    except Exception:
                        # there is record for original(reschedule). So just delete for removed address!
                        session.rollback()
                        session.query(CustomerScheduleAddress). \
                            filter(CustomerScheduleAddress.customer_address_id == removed_customer_address.id).delete()

                    session.delete(removed_customer_address)
                    session.query(CustomerAddress).filter(CustomerAddress.id == original_customer_address.id).update(
                        {"used_count": (original_customer_address.used_count + removed_customer_address.used_count)})
                    session.commit()
            except Exception as ex:
                print("----------ERROR----------")
                print(ex)
                print(addr1.id)
                print(addr2.id)
                session.rollback()
                continue

        address_count -= len(removed_indices)
        removed_indices.sort(reverse=True)
        for index in removed_indices:
            del addresses[index]

    session.close()
    print("Customer id : " + str(customer_id) + " is done!")


session = Session()
customer_address_counts = func.count(CustomerAddress.customer_id)
customer_ids_list = session.query(CustomerAddress.customer_id).group_by(CustomerAddress.customer_id)\
    .having(customer_address_counts > 1).order_by(customer_address_counts.desc()).all()

customer_addresses_relation_tuple_dict, customer_address_ids_dict = \
    fetch_customer_address_records(session, customer_ids_list)
session.close()

executor = ThreadPoolExecutor(max_workers=4)

for customer_id in customer_ids_list:
    customer_id = customer_id[0]
    executor.submit(merge_customer_addresses, customer_id, customer_address_ids_dict,
                    customer_addresses_relation_tuple_dict)

executor.shutdown()
Session.remove()
