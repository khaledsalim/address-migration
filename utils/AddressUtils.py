# -*- coding: utf-8 -*-

from re import compile, IGNORECASE
from json import dumps
from sys import maxint
import logging
from math import sin, cos, sqrt, atan2, radians
from collections import defaultdict
from re import split, sub
from decimal import Decimal
from datetime import datetime

# approximate radius of earth in meters
R = 6371000

camel_pat = compile(r'([A-Z])')
under_pat = compile(r'_([a-z])')
address_clean_pattern = compile(r"[\n\"]|<br>|<br/>|</>", flags=IGNORECASE)


def underscore_to_camel(name):
    return under_pat.sub(lambda x: x.group(1).upper(), name)


def convert_snake_to_camel(d):
    new_d = {}
    for k, v in d.items():
        new_d[underscore_to_camel(k)] = convert_snake_to_camel(v) if isinstance(v, dict) else v
    return new_d


def to_json(source, encoder=False):
    if encoder:
        return dumps(convert_snake_to_camel(source.__dict__), default=alchemy_encoder)
    return dumps(source, default=lambda instance: convert_snake_to_camel(instance.__dict__), sort_keys=True, indent=4)


def alchemy_encoder(obj):
    """JSON encoder function for SQLAlchemy special classes."""
    if isinstance(obj, datetime):
        return obj.isoformat()
    elif isinstance(obj, Decimal):
        return float(obj)


def setup_c2c_loggers():
    c2c_failed_logger = logging.getLogger('c2c-errors')
    c2c_failed_handler = logging.FileHandler('c2c_failed_addresses.log', mode='w+')
    c2c_failed_formatter = logging.Formatter('%(asctime)s %(message)s')
    c2c_failed_handler.setFormatter(c2c_failed_formatter)
    c2c_failed_logger.addHandler(c2c_failed_handler)
    c2c_failed_logger.setLevel(logging.INFO)

    c2c_last_processed_logger = logging.getLogger('c2c-last-processed')
    c2c_last_processed_handler = logging.FileHandler(filename='c2c_last_processed.log', mode='a')
    c2c_last_processed_formatter = logging.Formatter('%(message)s')
    c2c_last_processed_handler.setFormatter(c2c_last_processed_formatter)
    c2c_last_processed_logger.addHandler(c2c_last_processed_handler)
    c2c_last_processed_logger.setLevel(logging.INFO)

    return c2c_last_processed_logger, c2c_failed_logger


def setup_b2c_loggers():
    b2c_failed_logger = logging.getLogger('b2c-errors')
    b2c_failed_handler = logging.FileHandler('b2c_failed_addresses.log', mode='a')
    b2c_failed_formatter = logging.Formatter('%(asctime)s %(message)s')
    b2c_failed_handler.setFormatter(b2c_failed_formatter)
    b2c_failed_logger.addHandler(b2c_failed_handler)
    b2c_failed_logger.setLevel(logging.INFO)

    b2c_last_processed_logger = logging.getLogger('b2c-last-processed')
    b2c_last_processed_handler = logging.FileHandler(filename='b2c_last_processed.log', mode='w+')
    b2c_last_processed_formatter = logging.Formatter('%(message)s')
    b2c_last_processed_handler.setFormatter(b2c_last_processed_formatter)
    b2c_last_processed_logger.addHandler(b2c_last_processed_handler)
    b2c_last_processed_logger.setLevel(logging.INFO)

    b2c_max_processed_logger = logging.getLogger('b2c-max-processed')
    b2c_max_processed_handler = logging.FileHandler(filename='b2c_max_processed.log', mode='w+')
    b2c_max_processed_formatter = logging.Formatter('%(message)s')
    b2c_max_processed_handler.setFormatter(b2c_max_processed_formatter)
    b2c_max_processed_logger.addHandler(b2c_max_processed_handler)
    b2c_max_processed_logger.setLevel(logging.INFO)

    return b2c_last_processed_logger, b2c_max_processed_logger, b2c_failed_logger


def initialize_last_processed_address():
    try:
        with open('c2c_last_processed.log', "r") as filename:
            filename.seek(0)
            c2c_last_processed_id = filename.read()
            filename.close()
        c2c_last_processed_id = int(c2c_last_processed_id)
    except (IOError, ValueError):
        c2c_last_processed_id = 0

    try:
        with open('b2c_last_processed.log', "r") as filename:
            filename.seek(0)
            b2c_last_processed_id = filename.read()
            filename.close()
        b2c_last_processed_id = int(b2c_last_processed_id)
    except (IOError, ValueError):
        b2c_last_processed_id = 0

    try:
        with open('b2c_max_processed.log', "r") as filename:
            filename.seek(0)
            b2c_max_processed_id = filename.read()
            filename.close()
        b2c_max_processed_id = int(b2c_max_processed_id)
    except (IOError, ValueError):
        b2c_max_processed_id = maxint

    return c2c_last_processed_id, b2c_last_processed_id, b2c_max_processed_id


def calculate_addresses_distance(lat1, lng1, lat2, lng2):
    """
    :param lat1:
    :param lng1:
    :param lat2:
    :param lng2:
    :return: distance between two locations in meters
    """
    dlat = radians(lat2 - lat1)
    dlng = radians(lng2 - lng1)

    a = sin(dlat / 2) ** 2 + cos(radians(lat1)) * cos(radians(lat2)) * sin(dlng / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    dist = R * c
    return dist


def _extract_numbers_from_str_list(words):
    nums = []
    for w in words:
        if w.isdigit():
            nums.append(w)

    return nums


def _compare_nums(nums1, nums2):
    """"
    Make sure any nums is a subset from the another.
    """
    if len(nums1) > len(nums2):
        return _compare_nums(nums2, nums1)

    # len nums1 < len nums2
    for num in nums1:
        if num not in nums2:
            return False

    return True


def _normalize(str):
    str = str.lower()
    str = sub("[إأٱآ]", "ا", str)
    str = sub("ى", "ي", str)
    str = sub("[ؤئ]", "ء", str)
    str = sub("ة", "ه", str)

    return str


def calculate_addresses_similarity(address_1, address_2):
    address_1 = _normalize(address_1)
    address_2 = _normalize(address_2)
    words1 = split(' |,|;|-|_', address_1)
    words2 = split(' |,|;|-|_', address_2)
    nums1 = _extract_numbers_from_str_list(words1)
    nums2 = _extract_numbers_from_str_list(words2)
    is_numbers_identical = _compare_nums(nums1, nums2)
    if not is_numbers_identical:
        return 0

    if len(words1) > len(words2):
        max_len = len(words1)
    else:
        max_len = len(words2)
    dist = float(_calculate_sim(words1, words2))
    percentage = (max_len - dist) / max_len
    return percentage


def _calculate_sim(s1, s2):
    """
    Use jellyfish implementation for damerau_levenshtein_distance, but for word based distance
    :param s1:
    :param s2:
    :return: damerau_levenshtein_distance similarity
    """
    len1 = len(s1)
    len2 = len(s2)
    infinite = len1 + len2

    # character array
    da = defaultdict(int)

    # distance matrix
    score = [[0] * (len2 + 2) for x in range(len1 + 2)]

    score[0][0] = infinite
    for i in range(0, len1 + 1):
        score[i + 1][0] = infinite
        score[i + 1][1] = i
    for i in range(0, len2 + 1):
        score[0][i + 1] = infinite
        score[1][i + 1] = i

    for i in range(1, len1 + 1):
        db = 0
        for j in range(1, len2 + 1):
            i1 = da[s2[j - 1]]
            j1 = db
            cost = 1
            if s1[i - 1] == s2[j - 1]:
                cost = 0
                db = j

            score[i + 1][j + 1] = min(score[i][j] + cost,
                                      score[i + 1][j] + 1,
                                      score[i][j + 1] + 1,
                                      score[i1][j1] + (i - i1 - 1) + 1 + (j - j1 - 1))
        da[s1[i - 1]] = i

    return score[len1 + 1][len2 + 1]


def clean_address_line(address_line):
    address_line = address_line.strip()
    address_line = address_clean_pattern.sub("", address_line)
    return address_line


def parse_b2c_customer_name(name):
    first_name = None
    last_name = None
    name = name.strip(' \t\n?.*#$&')
    if name:
        name.replace('?*#$&', '')
        if name:
            name = name.split()
            if len(name) > 1:
                first_name = ' '.join(name[:-1])
                last_name = name[-1]
            else:
                first_name = name[0]
                last_name = '.'
    return first_name, last_name
