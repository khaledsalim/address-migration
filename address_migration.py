#! /usr/bin/python
# -*- coding: UTF-8 -*-

from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from model.AddressMW import AddressMW, ScheduleMW
from model.Address import Address
from model.Phone import Phone
from model.Customer import Customer
from model.CustomerPhone import CustomerPhone
from model.CustomerAddress import CustomerAddress
from model.CustomerScheduleAddress import CustomerScheduleAddress
from requests import get, post
import logging
import atexit
from utils.AddressUtils import to_json, setup_c2c_loggers, setup_b2c_loggers, initialize_last_processed_address, \
    clean_address_line, parse_b2c_customer_name
from argparse import ArgumentParser

engine = create_engine("postgresql://falcon:falcon123@mw-read.cfrk3zxk66i3.ap-southeast-1.rds.amazonaws.com/fetchrdb",
                       encoding='utf8')
Session = sessionmaker(bind=engine)

session = Session()

zones_token = 'fec7b00b-fe3f-43ab-b21c-01d2cb50bc77'

address_service_url = 'http://amaken.stag.fetchr.us'

starting_from = '2017-08-01'

batch_size = None

c2c_last_processed_address, b2c_last_processed_address, b2c_max_processed_address = initialize_last_processed_address()
c2c_last_processed_logger, c2c_failed_logger = setup_c2c_loggers()
b2c_last_processed_logger, b2c_max_processed_logger, b2c_failed_logger = setup_b2c_loggers()

total_records = 0
total_empty_address = 0
total_empty_phone = 0
total_empty_name = 0
total_outside_zone = 0
total_failed = 0
total_succeeded = 0
total_duplicate_address = 0
total_duplicate_customer = 0
total_duplicate_customer_address = 0


def get_address_country_city(latitude, longitude):
    global total_outside_zone
    query_parameters = {'latitude': latitude, 'longitude': longitude, 'excludeGeoJson': True}
    response = get(url='http://zones.fetchr.us/api/zoneLayers/default/zones/', params=query_parameters, headers={
        'Authorization': 'Bearer {}'.format(zones_token)})
    if response.status_code != 200:
        total_outside_zone += 1
        raise Exception("Couldn't connect to zones: {}".format(response.status_code))
    response_json = response.json()
    return response_json['data'][0]['countryCode'], response_json['data'][0]['cityCode']


def get_address_service_headers():
    headers = {'Content-type': 'application/json', 'Accept-Charset': 'UTF-8',
               'Authorization': 'Bearer 357c9366-5dc5-4c0b-a157-0104a9c5f6a2'}
    return headers


def create_customer(customer):
    global total_duplicate_customer
    customer_json = to_json(customer)
    response = post(url=address_service_url + '/api/customers/', data=customer_json,
                    headers=get_address_service_headers())
    if response.status_code == 409:
        total_duplicate_customer += 1
    elif response.status_code != 200:
        print(response.json())
        raise Exception("Couldn't create customer: {}".format(response.status_code))

    response_json = response.json()
    return response_json['data']


def create_address(address):
    global total_duplicate_address
    address_json = to_json(address, encoder=True)
    response = post(url=address_service_url + '/api/addresses/', data=address_json,
                    headers=get_address_service_headers())
    if response.status_code == 409:
        total_duplicate_address += 1
    elif response.status_code != 200:
        print(response.json())
        raise Exception("Couldn't create address: {}".format(response.status_code))

    response_json = response.json()
    return response_json['data']


def create_customer_address(customer_address):
    global total_duplicate_customer_address
    customer_address_json = to_json(customer_address, encoder=True)
    response = post(url=address_service_url + '/api/customer_addresses/', data=customer_address_json,
                    headers=get_address_service_headers())
    if response.status_code == 409:
        total_duplicate_customer_address += 1
    if response.status_code != 200:
        print(response.json())
        raise Exception("Couldn't create customer address: {}".format(response.status_code))


def add_schedule_address(customer_id, scheduling_number, transit_type, address_id):
    customer_schedule_address = CustomerScheduleAddress(customer_id=customer_id, scheduling_number=scheduling_number,
                                                        transit_type=transit_type, address_id=address_id)
    customer_schedule_address_json = to_json(customer_schedule_address, encoder=True)
    response = post(url=address_service_url + '/api/customer_schedule_address/',
                    data=customer_schedule_address_json, headers=get_address_service_headers())
    if response.status_code != 200:
        raise Exception("Couldn't create customer schedule address: {}".format(response.status_code))


def process_address(latitude, longitude, mobile_number, is_mobile_verified, first_name, last_name, address_line,
                    address_notes, address_label, is_favorite, is_active):
    country_code, city_code = get_address_country_city(latitude=latitude, longitude=longitude)

    phone = Phone(phone_number=mobile_number, is_working=True)

    customer_phone = CustomerPhone(phone=phone, is_active=is_mobile_verified)

    customer = Customer(first_name=first_name, last_name=last_name,
                        customer_phones=[customer_phone])
    customer_id = create_customer(customer=customer)
    customer.customer_id = customer_id

    address = Address(address=address_line, latitude=latitude, longitude=longitude,
                      country_code=country_code, city_code=city_code)
    address_id = create_address(address=address)
    address.address_id = address_id

    customer_address = CustomerAddress(customer=customer, address=address, label=address_label,
                                       notes=address_notes, is_favorite=is_favorite, is_active=is_active)
    create_customer_address(customer_address=customer_address)

    return customer_id, address_id


def migrate_c2c_addresses():
    global c2c_last_processed_address
    global total_records
    global total_empty_phone
    global total_empty_address
    global total_succeeded
    global total_failed

    result_length = batch_size
    while result_length != 0:
        query = session.query(AddressMW).filter(AddressMW.id > c2c_last_processed_address).order_by(
            AddressMW.id.asc()).limit(batch_size)
        result_length = query.count()
        total_records += 1
        for address_mw in query:
            try:
                c2c_last_processed_address = address_mw.id
                latitude = address_mw.latitude
                longitude = address_mw.longitude

                user = address_mw.user
                if not user:
                    c2c_failed_logger.log(level=logging.ERROR,
                                          msg="Invalid user for address {}".format(address_mw.id))
                    continue
                mobile_number = user.mobile_number
                if not (mobile_number and mobile_number.strip()):
                    total_empty_phone += 1
                    c2c_failed_logger.log(level=logging.ERROR,
                                          msg="Invalid phone number for address {}".format(address_mw.id))
                    continue
                mobile_number = mobile_number.strip()

                first_name = user.first_name
                last_name = user.last_name
                is_mobile_verified = user.is_mobile_verified
                address_line = address_mw.address
                address_label = address_mw.tag
                address_notes = address_mw.comments
                is_favorite = address_mw.is_favorite
                is_active = address_mw.is_active

                if not (address_line and address_line.strip()):
                    total_empty_address += 1
                    c2c_failed_logger.log(level=logging.ERROR,
                                          msg="invalid address line for address {}".format(address_mw.id))
                    continue

                address_line = clean_address_line(address_line)
                process_address(latitude=latitude, longitude=longitude, mobile_number=mobile_number,
                                is_mobile_verified=is_mobile_verified, first_name=first_name, last_name=last_name,
                                address_line=address_line, address_notes=address_notes, address_label=address_label,
                                is_favorite=is_favorite, is_active=is_active)
                print('Address creation succeeded: {}'.format(address_mw.id))
                total_succeeded += 1

            except Exception as e:
                total_failed += 1
                c2c_failed_logger.log(level=logging.ERROR, msg="Address {} failed: {}".format(address_mw.id, e.message))


def migrate_b2c_addresses():
    global b2c_last_processed_address
    global total_records
    global total_empty_phone
    global total_empty_name
    global total_empty_address
    global total_succeeded
    global total_failed
    result_length = batch_size
    while result_length != 0:
        query = session.query(ScheduleMW).filter(
            and_(ScheduleMW.id > b2c_last_processed_address, ScheduleMW.status == 4, ScheduleMW.transit_type == 1,
                 ScheduleMW.latitude != 0, ScheduleMW.longitude != 0), ScheduleMW.schedule_date >= starting_from) \
            .order_by(ScheduleMW.id.asc()).limit(batch_size)
        result_length = query.count()
        for schedule_mw in query:
            try:
                if schedule_mw.id >= b2c_max_processed_address:
                    result_length = 0
                    break
                total_records += 1
                b2c_last_processed_address = schedule_mw.id
                latitude = schedule_mw.latitude
                longitude = schedule_mw.longitude
                order = schedule_mw.order
                if not order:
                    total_failed += 1
                    raise Exception("Invalid order for address {}".format(schedule_mw.id))
                mobile_number = order.mobile_number
                name = order.name
                first_name, last_name = parse_b2c_customer_name(name)
                if not first_name:
                    total_empty_name += 1
                    total_failed += 1
                    b2c_failed_logger.log(level=logging.ERROR,
                                          msg="Invalid customer name for address {}".format(schedule_mw.id))
                    continue
                is_mobile_verified = True
                address_line = schedule_mw.address
                address_label = None
                address_notes = order.comments
                is_favorite = False
                is_active = True

                if not (mobile_number and mobile_number.strip()):
                    total_empty_phone += 1
                    b2c_failed_logger.log(level=logging.ERROR,
                                          msg="Invalid phone number for address {}".format(schedule_mw.id))
                    continue
                mobile_number = mobile_number.strip()

                if not (address_line and address_line.strip()):
                    total_empty_address += 1
                    total_failed += 1
                    b2c_failed_logger.log(level=logging.ERROR,
                                          msg="Invalid address line for address {}".format(schedule_mw.id))
                    continue

                address_line = clean_address_line(address_line)
                customer_id, address_id = process_address(latitude=latitude, longitude=longitude,
                                                          mobile_number=mobile_number,
                                                          is_mobile_verified=is_mobile_verified, first_name=first_name,
                                                          last_name=last_name,
                                                          address_line=address_line, address_notes=address_notes,
                                                          address_label=address_label,
                                                          is_favorite=is_favorite, is_active=is_active)
                add_schedule_address(customer_id=customer_id, scheduling_number=schedule_mw.tracking_number,
                                     transit_type='ForwardDelivery', address_id=address_id)
                print('Address creation succeeded: {}'.format(schedule_mw.id))
                total_succeeded += 1
            except Exception as e:
                total_failed += 1
                b2c_failed_logger.log(level=logging.ERROR,
                                      msg="Address {} failed: {}".format(schedule_mw.id, e.message))


@atexit.register
def cleanup():
    global c2c_last_processed_logger
    global b2c_last_processed_logger
    global b2c_max_processed_logger
    c2c_last_processed_logger.log(level=logging.INFO, msg='{}'.format(c2c_last_processed_address))
    c2c_failed_logger.handlers[0].flush()

    b2c_last_processed_logger.log(level=logging.INFO, msg='{}'.format(b2c_last_processed_address))
    b2c_max_processed_logger.log(level=logging.INFO, msg='{}'.format(b2c_max_processed_address))
    b2c_failed_logger.handlers[0].flush()

    print('Total processed records: {}'.format(total_records))
    print('\tTotal successful records: {}'.format(total_succeeded))
    print('\tTotal duplicate customer: {}'.format(total_duplicate_customer))
    print('\tTotal duplicate addresses: {}'.format(total_duplicate_address))
    print('\tTotal duplicate customer address: {}'.format(total_duplicate_customer_address))
    print('\tTotal failed records: {}'.format(total_failed))
    print('\t\tTotal records without valid name: {}'.format(total_empty_name))
    print('\t\tTotal records without valid phone: {}'.format(total_empty_phone))
    print('\t\tTotal records without valid address: {}'.format(total_empty_address))
    print('\t\tTotal records without valid zone: {}'.format(total_outside_zone))


def main():
    parser = ArgumentParser(description='Address migration tool from middleware to the Address service')
    parser.add_argument('-c', '--c2c', dest='c2c', action='store_true', help='Migrate C2C addresses')
    parser.add_argument('-b', '--b2c', dest='b2c', action='store_true', help='Migrate B2C addresses')
    parser.add_argument('-s', '--batch-size', dest='batch_size', type=int, default=1000, help='Batch size')
    args = parser.parse_args()

    if args.batch_size > 0:
        global batch_size
        batch_size = args.batch_size

    if not (args.c2c or args.b2c):
        parser.error('At least one argument has to be provided')

    if args.c2c:
        migrate_c2c_addresses()

    if args.b2c:
        migrate_b2c_addresses()


if __name__ == "__main__":
    main()
