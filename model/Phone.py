class Phone:
    """Phone model for the Address service

        Attributes:
            phone_number: String contains the phone number
            is_working: Boolean indicated if the phone is working or not, default is True
        """

    def __init__(self, phone_number, is_working):
        if not (phone_number or phone_number.strip()):
            raise Exception('Failed to add phone number: cannot be empty')

        self.phone_number = phone_number
        self.is_working = is_working
