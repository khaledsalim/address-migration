from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class CustomerScheduleAddress(Base):
    """Address model for the Address service

            Attributes:
                address_id: Address external ID
                customer_id: Customer external ID
                scheduling_number: Schedule external ID
                transit_type: Schedule transit type
            """
    __tablename__ = 'customer_schedule_address'
    id = Column('id', Integer, primary_key=True)
    customer_address_id = Column('customer_address_id', Integer)

    def __init__(self, customer_id, scheduling_number, transit_type, address_id):
        self.customer_id = customer_id
        self.scheduling_number = scheduling_number
        self.transit_type = transit_type
        self.address_id = address_id
