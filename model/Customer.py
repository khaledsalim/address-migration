class Customer:
    """Customer model for the Address service

        Attributes:
            first_name: String for first name
            last_name: String for last name
            customer_phones: Array of Phone objects
        """

    def __init__(self, first_name, last_name, customer_phones):
        self.first_name = first_name
        self.last_name = last_name
        self.customer_phones = customer_phones
