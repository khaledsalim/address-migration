# -*- coding: UTF-8 -*-
from __future__ import unicode_literals
from re import search, IGNORECASE, UNICODE
from sqlalchemy import Column, String, DateTime, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class CustomerAddress(Base):
    """Customer Address model for the Address service

        Attributes:
            customer_id: String customer_id
            address_id: String address_id
            type: String Home, Business, etc...
            notes: String notes about the customer phone
            is_active: Boolean indicated if the customer phone is active or not, default is True
            is_favorite: Boolean indicated if the address is the customer's favorite, default is False
        """
    __tablename__ = 'customer_address'
    # Only read customer and address ids.
    id = Column('id', Integer, primary_key=True)
    customer_id = Column('customer_id', Integer)
    address_id = Column('address_id', Integer)
    used_count = Column('used_count', Integer)

    def __init__(self, customer, address, label, notes, is_favorite, is_active):
        self.customer_id = customer.customer_id
        self.address_id = address.address_id
        self.label = label

        if notes and notes:
            self.notes = notes

        if is_favorite:
            self.is_favorite = True
        else:
            self.is_favorite = False

        if is_active:
            self.is_active = True
        else:
            self.is_active = False

        address_tag = ''
        if address.address_line:
            address_tag += ' ' + address.address_line

        if label:
            address_tag += ' ' + label

        if notes:
            address_tag += ' ' + notes

        self.type = self.deduce_address_type(address_tag)

    @staticmethod
    def deduce_address_type(tag):
        if tag is not None:
            home_strings = ['home', 'house', 'flat', 'villa', 'فيلا', 'منزل', 'شقة', 'بيت', 'هوم']
            for home_string in home_strings:
                if search(home_string, tag, IGNORECASE | UNICODE):
                    return 'Home'
        return 'Business'
