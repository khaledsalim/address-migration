from sqlalchemy import Column, Numeric, String, DateTime, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Address(Base):
    """Address model for the Address service

        Attributes:
            address_line: Address line 1
            country_code: 2 characters ISO country code
            city_code: 3 characters city code
            latitude: Address latitude
            longitude: Address longitude
        """
    __tablename__ = 'address'
    id = Column('id', Integer, primary_key=True)
    address_id = Column('address_id', String(64))
    address_line = Column('address_line', String())
    country_code = Column('country_code', String(2))
    city_code = Column('city_code', String(3))
    area = Column('area', String(45))
    street = Column('street', String())
    building = Column('building', String())
    latitude = Column('latitude', Numeric(10, 7))
    longitude = Column('longitude', Numeric(10, 7))
    created_at = Column('created_at', DateTime)
    modified_at = Column('modified_at', DateTime)
    notes = Column('notes', String(255))
    zip_code = Column('zip_code', String(10))
    address_line_2 = Column('address_line_2', String(100))

    def __init__(self, address, latitude, longitude, country_code, city_code):
        self.address_line = address
        self.country_code = country_code
        self.city_code = city_code
        self.latitude = float(latitude)
        self.longitude = float(longitude)
