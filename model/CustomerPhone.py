class CustomerPhone:
    """Customer Phone model for the Address service

        Attributes:
            phone: Phone object
            type: Phone type: Home, Mobile, etc...
            is_active: Boolean indicated if the customer phone is active or not, default is True
        """

    def __init__(self, phone, is_active):
        self.phone = phone
        self.type = 'Mobile'
        self.is_active = is_active
