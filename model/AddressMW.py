from sqlalchemy import Column, Numeric, String, DateTime, Integer, ForeignKey, Boolean, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class UserMW(Base):
    __tablename__ = 'user_profile'
    id = Column('id', Integer, primary_key=True)
    user_id = Column('user_id', Integer)
    first_name = Column('first_name', String(35))
    last_name = Column('last_name', String(35))
    mobile_number = Column('mobile_number', String(15))
    is_mobile_verified = Column('is_mob_verified', Boolean)
    country_code = Column('country_code', String(2))


class AddressMW(Base):
    __tablename__ = 'address'
    id = Column('id', Integer, primary_key=True)
    latitude = Column('latitude', Numeric(10, 7))
    longitude = Column('longitude', Numeric(10, 7))
    tag = Column('tag', String(35))
    address = Column('address', String(255))
    comments = Column('comments', String(255))
    is_active = Column('status', Integer)
    is_favorite = Column('is_favourite', Boolean)
    created_at = Column('created_date', DateTime)
    modified_at = Column('updated_date', DateTime)
    user_id = Column(Integer, ForeignKey('user_profile.user_id'))
    user = relationship('UserMW', foreign_keys=[user_id], primaryjoin="UserMW.id == AddressMW.user_id", lazy='joined')


class OrderMW(Base):
    __tablename__ = 'orders'
    id = Column('id', Integer, primary_key=True)
    tracking_number = Column('tracking_id', String(20))
    name = Column('receiver_name', String(255))
    mobile_number = Column('receiver_phone', String(20))
    comments = Column('receiver_comment', String(255))


class ScheduleMW(Base):
    __tablename__ = 'transit_schedule'
    id = Column('id', Integer, primary_key=True)
    tracking_number = Column('tracking_id', String(20))
    user_id = Column(Integer, ForeignKey('user_profile.user_id'))
    address = Column('address', String(255))
    latitude = Column('latitude', Numeric(10, 7))
    longitude = Column('longitude', Numeric(10, 7))
    schedule_date = Column('schedule_date', Date)
    status = Column('status', Integer)
    transit_type = Column('type', Integer)
    order = relationship('OrderMW', primaryjoin="OrderMW.tracking_number == foreign(ScheduleMW.tracking_number)",
                         lazy='joined')
